export const TASK_INPUT_CHANGE = "Sự kiện thay đổi input khi nhập task";
export const TASK_ADD_CLICKED = "Sự kiện khi nhấn nút Add Task, thêm task mới vào danh sách";
export const TASK_TOGGLE_CLICKED = "Sự kiện khi nhấn vào 1 task thì thay đổi trạng thái của task đó"